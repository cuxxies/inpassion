package id.augmented.inpassion.Controller;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hendr on 4/24/2017.
 */

public class DataController {
    String TAG = "DataController";
    private DatabaseReference mDatabase;
    private JSONArray projects;
    private JSONArray rooms;

    private JSONArray roomimages;
    private JSONArray users;
    private JSONArray comments;
    private JSONArray orders;
    private static DataController instance = null;
    private static Context mContext;

    protected DataController() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //dataSnapshot.getValue()
                // String value = dataSnapshot.getValue(String.class);
                JSONObject json = new JSONObject((Map) dataSnapshot.getValue());
                Log.d(TAG, "Value is: " + json.toString());
                applyDataChangeResult(json);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public static DataController getInstance(Context context) {
        if (instance == null) {
            instance = new DataController();
        }
        mContext = context;
        return instance;
    }

    private void applyDataChangeResult(JSONObject jsonObject) {
        try {
            projects = jsonObject.getJSONArray("projects");
            rooms = jsonObject.getJSONArray("rooms");
            roomimages = jsonObject.getJSONArray("roomimages");
            users = jsonObject.getJSONArray("users");
            comments = jsonObject.getJSONArray("comments");
            orders = jsonObject.getJSONArray("orders");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    public JSONArray getProjects() {
        return projects;
    }

    public JSONArray getRooms() {
        return rooms;
    }

    public JSONArray getUsers() {
        return users;
    }

    public JSONArray getComments() {
        return comments;
    }

    public JSONArray getOrders() {
        return orders;
    }

    public JSONArray getRoomimages() {
        return roomimages;
    }

    public void addNewUser(JSONObject userObject){
        String jsonString = userObject.toString(); //set to json string
        Map<String, Object> jsonMap = new Gson().fromJson(jsonString, new TypeToken<HashMap<String, Object>>() {}.getType());

        //mDatabase.child("users").child().setValue(jsonMap);
    }
}

