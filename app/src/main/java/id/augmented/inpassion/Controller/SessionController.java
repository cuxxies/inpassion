package id.augmented.inpassion.Controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONObject;

/**
 * Created by hendr on 4/25/2017.
 */

public class SessionController {
    String USER_LOGGED_BOOL_KEY = "isLoggedIn";
    String USER_ID_KEY = "userID";
    private static SessionController instance = null;
    private static Context mContext;
    public static SessionController getInstance(Context context) {
        if(instance == null) {
            instance = new SessionController();
        }
        mContext = context;
        return instance;
    }

    public boolean isLoggedIn()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        return prefs.getBoolean(USER_LOGGED_BOOL_KEY, false);
    }

    public void login(JSONObject jsonObject)
    {

    }

}
