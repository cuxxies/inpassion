package id.augmented.inpassion.Controller;

/**
 * Created by hendr on 4/24/2017.
 */

public interface DataControllerInterface {
    public void onDataChanged(String result);
    public void onCancelled(String result);
}
